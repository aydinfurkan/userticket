﻿using System;
using System.Collections.Generic;
using System.Threading.Tasks;
using Microsoft.AspNetCore.Mvc;
using WebApplication.Model;
using WebApplication.Models;
using WebApplication.Services;
using WebApplication.Services.Interfaces;

namespace WebApplication.Controllers
{
    [Route("api/[controller]")]
    [ApiController]
    public class TicketController : ControllerBase
    {
        private readonly ITicketServices _ticketServices;
        private readonly IUserServices _userServices;

        public TicketController(ITicketServices ticketServices, IUserServices userServices)
        {
            _ticketServices = ticketServices;
            _userServices = userServices;
        }

        [HttpGet]
        public ActionResult<List<Ticket>> Get()
        {
            return _ticketServices.Get();
        }

        [HttpGet("{id}", Name = "GetTicket")]
        public ActionResult<Ticket> Get(Guid id)
        {
            return _ticketServices.Get(id);
        }

        [HttpPost]
        public ActionResult<Ticket> Create(TicketDto ticketDto)
        {
            var creator = _userServices.Get(ticketDto.CreatorId);

            var newTicket = _ticketServices.Create(new Ticket(ticketDto));
            
            creator.TicketIds.Add(newTicket.Id);
            _userServices.Update(creator.Id, creator);

            return CreatedAtRoute("GetTicket", new {id = newTicket.Id.ToString()}, newTicket);
        }

        [HttpPut("{id}")]
        public IActionResult Update(Guid id, TicketDto ticketDto)
        {
            var ticket = _ticketServices.Get(id);

            if (!ticket.CreatorId.Equals(ticketDto.CreatorId))
            {
                return Forbid();
            }

            ticket.Update(ticketDto);
            _ticketServices.Update(id, ticket);

            return NoContent();
        }

        [HttpPut("response/{id}")]
        public IActionResult UpdateTicketAnswer(Guid id, string answer)
        {
            var ticket = _ticketServices.Get(id);

            ticket.Answer = answer;
            ticket.IsOpen = false;
            _ticketServices.Update(ticket.Id, ticket);
            return NoContent();

        }
        
        [HttpDelete("{id}")]
        public IActionResult Delete(Guid id)
        {
            var ticket = _ticketServices.Get(id);

            var creator = _userServices.Get(ticket.CreatorId);

            if (creator != null)
            {
                creator.TicketIds.Remove(ticket.Id);
                _userServices.Update(creator.Id, creator);
            }

            _ticketServices.Remove(ticket.Id);
            
            return NoContent();
        }
        
    }
}
﻿using System;
using System.Collections.Generic;
using System.Threading.Tasks;
using Microsoft.AspNetCore.Mvc;
using WebApplication.Http;
using WebApplication.Model;
using WebApplication.Models;
using WebApplication.Services;
using WebApplication.Services.Interfaces;

namespace WebApplication.Controllers
{
    [Route("api/[controller]")]
    [ApiController]
    public class UserController : ControllerBase
    {
        private readonly IUserServices _userService;
        private readonly ITicketServices _ticketServices;

        public UserController(IUserServices userService, ITicketServices ticketServices)
        {
            _userService = userService;
            _ticketServices = ticketServices;
        }

        [HttpGet]
        public ActionResult<List<User>> Get()
        {
            return _userService.Get();
        }
        
        [HttpGet("{id}", Name = "GetUser")]
        public ActionResult<User> Get(Guid id)
        {
            return _userService.Get(id);
        }
        
        [HttpPost]
        public ActionResult<User> Create(UserDto userDto)
        {
            var newUser = _userService.Create(new User(userDto));

            return CreatedAtRoute("GetUser", new { id = newUser.Id.ToString() }, newUser);
        }
        
        [HttpPut("{id}")]
        public IActionResult Update(Guid id, UserDto userDto)
        {
            var user = _userService.Get(id);

            user.Update(userDto);
            _userService.Update(id, user);

            return NoContent();
        }
        
        [HttpDelete("{id}")]
        public IActionResult Delete(Guid id)
        {
            var user = _userService.Get(id);

            _userService.Remove(user.Id);
            _ticketServices.RemoveAllByCreatorId(user.Id);

            return NoContent();
        }
        
        [HttpDelete("all")]
        public IActionResult Delete()
        {
            _userService.RemoveAll();
            _ticketServices.RemoveAll();
            return NoContent();
        }
        
    }
}
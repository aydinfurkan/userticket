﻿using System;
using WebApplication.Model;

namespace WebApplication.Models
{
    public class Ticket : BaseModel
    {
        public string Message { get; set; }
        public Guid CreatorId { get; set; }
        public bool IsOpen { get; set; }
        public string Answer { get; set; }

        public Ticket(TicketDto ticketDto)
        {
            Message = ticketDto.Message;
            CreatorId = ticketDto.CreatorId;
            IsOpen = true;
            Answer = "";
        }

        public void Update(TicketDto ticketDto)
        {
            Message = ticketDto.Message;
            CreatorId = ticketDto.CreatorId;
        }
    }
}
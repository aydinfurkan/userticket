﻿using System;
using System.Collections.Generic;
using WebApplication.Model;

namespace WebApplication.Models
{
    public class User : BaseModel
    {
        public string Username { get; set; }
        public string Password { get; set; }
        public string Email { get; set; }
        public List<Guid> TicketIds { get; set; }

        public User(UserDto userDto)
        {
            Username = userDto.Username;
            Password = userDto.Password;
            Email = userDto.Email;
            TicketIds = new List<Guid>();
        }

        public void Update(UserDto userDto)
        {
            Username = userDto.Username;
            Password = userDto.Password;
            Email = userDto.Email;
        }
    }
}
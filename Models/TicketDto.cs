﻿using System;
using System.ComponentModel.DataAnnotations;

namespace WebApplication.Models
{
    public class TicketDto
    {
        [Required]
        public string Message { get; set; }
        
        [Required]
        [RegularExpression("^(?!(00000000-0000-0000-0000-000000000000)$)", ErrorMessage = "Cannot use default Guid")]
        public Guid CreatorId { get; set; }
    }
}
﻿using System;
using System.Collections.Generic;
using MongoDB.Driver;
using WebApplication.DatabaseSettings;
using WebApplication.Model;
using WebApplication.Models;
using WebApplication.Repository;
using WebApplication.Repository.Interfaces;
using WebApplication.Services.Interfaces;

namespace WebApplication.Services
{
    public class UserServices : BaseServices<User>, IUserServices
    {
        public sealed override IRepository<User> Repository { get; set; }

        public UserServices(IMongoContext mongoContext, UserTicketDatabaseSettings databaseSettings)
        {
            Repository = new UserRepository(mongoContext, databaseSettings.UserCollectionName);
        }


    }
    
}
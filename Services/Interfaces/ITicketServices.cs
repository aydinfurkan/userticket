﻿using System;
using WebApplication.Model;
using WebApplication.Models;

namespace WebApplication.Services.Interfaces
{
    public interface ITicketServices : IService<Ticket>
    {
        public void RemoveAllByCreatorId(Guid creatorId);
    }
}
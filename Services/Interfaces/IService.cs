﻿using System;
using System.Collections.Generic;
using System.Threading.Tasks;
using WebApplication.Model;
using WebApplication.Models;
using WebApplication.Repository.Interfaces;

namespace WebApplication.Services.Interfaces
{
    public interface IService<T> where T : BaseModel
    {
        public IRepository<T> Repository { set; get; }
        public List<T> Get();
        public T Get(Guid id);
        public T Create(T document);
        public void Update(Guid id, T document);
        public void Remove(Guid id);

        public void RemoveAll();

    }
}
﻿using System;
using System.Collections.Generic;
using MongoDB.Driver;
using WebApplication.DatabaseSettings;
using WebApplication.Model;
using WebApplication.Models;
using WebApplication.Repository;
using WebApplication.Repository.Interfaces;
using WebApplication.Services.Interfaces;

namespace WebApplication.Services
{
    public class TicketServices : BaseServices<Ticket>, ITicketServices
    { 
        public sealed override IRepository<Ticket> Repository { get; set; }
        
        public TicketServices(IMongoContext mongoContext, UserTicketDatabaseSettings databaseSettings)
        {
            Repository = new TicketRepository(mongoContext, databaseSettings.TicketCollectionName); 
        }

        public void RemoveAllByCreatorId(Guid creatorId)
        {
            Repository.Delete(x => x.CreatorId == creatorId);
        }
    }
}
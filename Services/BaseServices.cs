﻿using System;
using System.Collections.Generic;
using System.Threading.Tasks;
using WebApplication.Http;
using WebApplication.Model;
using WebApplication.Models;
using WebApplication.Repository.Interfaces;
using WebApplication.Services.Interfaces;

namespace WebApplication.Services
{
    public abstract class BaseServices<T> : IService<T> where T : BaseModel
    {
        public abstract IRepository<T> Repository { get; set; }

        public List<T> Get()
        {
            return Repository.FindMany(x => true);
        }

        public T Get(Guid id)
        {
            var document = Repository.Find(x => x.Id==id);
            
            if(document == null)
                throw new HttpNotFound(id.ToString());
            
            return document;
        }

        public T Create(T document)
        {
            return Repository.Insert(document);
        }

        public void Update(Guid id, T document)
        {
            Repository.Replace(x => x.Id == id, document);
        }

        public void Remove(Guid id)
        {
            Repository.Delete(x => x.Id == id);
        }
        
        public void RemoveAll()
        {
            Repository.DeleteAll();
        }
    }
}
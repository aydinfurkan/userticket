﻿using System;
using System.Collections.Generic;
using System.Globalization;
using System.Linq.Expressions;
using System.Runtime.Serialization;
using MongoDB.Driver;
using WebApplication.Model;
using WebApplication.Models;
using WebApplication.Repository.Interfaces;

namespace WebApplication.Repository
{
    public class TicketRepository : MongoRepository<Ticket>, ITicketRepository
    {

        public TicketRepository(IMongoContext mongoContext, string collectionName) : base(mongoContext, collectionName)
        {
            
        }
        
    }
}
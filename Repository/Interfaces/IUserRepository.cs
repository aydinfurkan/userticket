﻿using WebApplication.Model;
using WebApplication.Models;

namespace WebApplication.Repository.Interfaces
{
    public interface IUserRepository : IRepository<User>
    {
    }
}
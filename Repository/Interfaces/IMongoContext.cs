﻿using System.Collections.Generic;
using MongoDB.Driver;

namespace WebApplication.Repository.Interfaces
{
    public interface IMongoContext
    {
        public IMongoCollection<T> GetCollection<T>(string collectionName);
    }
}
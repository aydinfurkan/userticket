﻿using System;
using System.Collections.Generic;
using System.Globalization;
using System.Runtime.Serialization;
using WebApplication.Model;
using WebApplication.Models;
using WebApplication.Repository.Interfaces;

namespace WebApplication.Repository
{
    public class UserRepository : MongoRepository<User>, IUserRepository
    {

        public UserRepository(IMongoContext mongoContext, string collectionName) : base(mongoContext, collectionName)
        {
        }
    }
}
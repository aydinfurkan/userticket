﻿namespace WebApplication.DatabaseSettings
{
    public class UserTicketDatabaseSettings
    {
        public string UserCollectionName { get; set; }
        public string TicketCollectionName { get; set; }
        public string ConnectionString { get; set; }
        public string DatabaseName { get; set; }
    }

}
﻿using FluentValidation;
using WebApplication.Model;
using WebApplication.Models;

namespace WebApplication.Validators
{
    public class UserValidator : AbstractValidator<UserDto>
    {
        public UserValidator()
        {
            RuleFor(x => x.Username).Length(5,30);
            RuleFor(x => x.Email).EmailAddress();
        }
    }
}
﻿using System.Data;
using FluentValidation;
using WebApplication.Model;
using WebApplication.Models;

namespace WebApplication.Validators
{
    public class TicketValidator : AbstractValidator<TicketDto>
    {
        public TicketValidator()
        {
            RuleFor(x => x.Message).Length(10,100);
            RuleFor(x => x.CreatorId).NotNull();
        }
    }
}
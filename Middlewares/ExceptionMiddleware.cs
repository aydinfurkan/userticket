﻿using System;
using System.Globalization;
using System.Net;
using System.Threading.Tasks;
using Microsoft.AspNetCore.Http;
using Microsoft.AspNetCore.Mvc;
using MongoDB.Driver;
using Newtonsoft.Json;
using WebApplication.Model;

namespace WebApplication.Middleware
{
    public class ExceptionMiddleware
    {
        private readonly RequestDelegate _next;

        public ExceptionMiddleware(RequestDelegate next)
        {
            _next = next;
        }

        public async Task InvokeAsync(HttpContext context)
        {
            try
            {
                await _next(context);
            }
            catch (HttpException httpException)
            {
                context.Response.StatusCode = (int) httpException.HttpStatusCode;
                context.Response.ContentType = "application/json";
                WriteClassIntoResponse(httpException.ConvertDto(), context);
            }
            catch (Exception exception)
            {
                Console.WriteLine("ASDASD"); // TODO Async controller
            }
        }

        private void WriteClassIntoResponse(object obj, HttpContext context)
        {
            context.Response.WriteAsync(JsonConvert.SerializeObject(obj)).Wait();
        }
    }
}
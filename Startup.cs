using System;
using System.Collections.Generic;
using FluentValidation;
using FluentValidation.AspNetCore;
using Microsoft.AspNetCore.Builder;
using Microsoft.AspNetCore.Hosting;
using Microsoft.Extensions.Configuration;
using Microsoft.Extensions.DependencyInjection;
using Microsoft.Extensions.Hosting;
using Microsoft.Extensions.Options;
using WebApplication.DatabaseSettings;
using WebApplication.Middleware;
using WebApplication.Model;
using WebApplication.Models;
using WebApplication.Repository;
using WebApplication.Repository.Interfaces;
using WebApplication.Services;
using WebApplication.Services.Interfaces;
using WebApplication.Validators;

namespace WebApplication
{
    public class Startup
    {
        public Startup(IConfiguration configuration)
        {
            Configuration = configuration;
        }

        public IConfiguration Configuration { get; }

        // This method gets called by the runtime. Use this method to add services to the container.
        public void ConfigureServices(IServiceCollection services)
        {
            services.Configure<UserTicketDatabaseSettings>(
                Configuration.GetSection(nameof(UserTicketDatabaseSettings)));
            
            services.AddSingleton(sp => sp.GetRequiredService<IOptions<UserTicketDatabaseSettings>>().Value);
            
            services.AddSingleton<IMongoContext, MongoContext>();
            services.AddSingleton<IUserServices, UserServices>();
            services.AddSingleton<ITicketServices, TicketServices>();

            services.AddMvc().AddFluentValidation();
            services.AddTransient<IValidator<UserDto>, UserValidator>();
            services.AddTransient<IValidator<TicketDto>, TicketValidator>();
            
            
            services.AddControllers();
        }

        // This method gets called by the runtime. Use this method to configure the HTTP request pipeline.
        public void Configure(IApplicationBuilder app, IWebHostEnvironment env)
        {
            if (env.IsDevelopment())
            {
                app.UseDeveloperExceptionPage();
            }

            app.UseMiddleware<ExceptionMiddleware>();

            app.UseHttpsRedirection();

            app.UseRouting();

            app.UseAuthorization();

            app.UseEndpoints(endpoints => { endpoints.MapControllers(); });
        }
    }
}